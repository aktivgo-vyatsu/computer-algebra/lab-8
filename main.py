from generator import *

if __name__ == '__main__':
    def generator_func(n, x0) -> float:
        # a = 7^5 or 7^13
        return (pow(7, 5) * x0) % n


    print('Линейный конгруэнтный метод:', linear_congruent_method(
        int(pow(2, 31) - 1), 1, generator_func, 8,
    ))

    print('Метод Фиббоначи с запаздыванием:', fibonacci_with_delay_method(
        4, 1, [0.7, 0.3, 0.9, 0.5], 10,
    ))

    print('Метод Блюма-Блюма-Шуба:', bbs(
        11, 19, 10,
    ))

    print('Метод Блюма-Блюма-Шуба (X10):', bbs_number(
        11, 19, 10,
    ))
