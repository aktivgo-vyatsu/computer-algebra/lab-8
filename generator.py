import math


def calculate_period(numbers: list) -> int:
    p = {}
    for num in numbers:
        if num in p:
            p[num] += 1
        p[num] = 1
    if all(v > 1 for v in p.values()):
        return min(p.values())
    return -1


def linear_congruent_method(n, x0, func, count) -> list:
    if x0 not in range(n):
        raise ValueError(f'x0 must be in range [0, {n - 1}]')
    result = [func(n, x0)]
    for i in range(1, count):
        result.append(func(n, result[i - 1]))
    print('Период:', calculate_period(result))
    return result


def fibonacci_with_delay_method(a, b, start, count) -> list:
    result = start
    for i in range(len(start), len(start) + count):
        current = result[i - a] - result[i - b]
        if result[i - a] <= result[i - b]:
            current += 1
        current = round(current, 2)
        result.append(current)
    print('Период:', calculate_period(result))
    return result


def bbs(p, q, count) -> dict:
    __bbs_validate(p, q)
    m = p * q
    x = __generate_x(m)
    x0 = pow(x, 2) % m
    result = [int(math.pow(x0, 2) % m)]
    for i in range(1, count):
        result.append(int(math.pow(result[i - 1], 2) % m))
    print('Период:', calculate_period(result))
    res = {}
    for r in result:
        res[r] = r & 1
    return res


def bbs_number(p, q, number) -> dict:
    __bbs_validate(p, q)
    m = p * q
    x = __generate_x(m)
    x0 = pow(x, 2) % m
    res = int(math.pow(x0, math.pow(2, number) % ((p - 1) * (q - 1))) % m)
    return {
        res: res & 1
    }


def __bbs_validate(p, q):
    if p % 4 != 3:
        raise ValueError(f'p validation error: {p} % 4 != 3')
    if q % 4 != 3:
        raise ValueError(f'q validation error: {q} % 4 != 3')


def __generate_x(m) -> float:
    x = 2
    while True:
        if math.gcd(m, x) == 1:
            return x
